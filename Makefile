VERSION=1.0.2

.PHONY: deploy
deploy:
	mvn clean deploy -Dmaven.test.skip=true -Drevision=${VERSION}