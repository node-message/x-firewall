<p align="center">
  <a href="https://gitee.com/node-message/x-firewall">
   <img src="./x-firewall-doc/assart/logo-banner.png" style="transform: scale(0.75);">
  </a>
</p>

<p align="center">
    x-firewall, 快速构建Web前置防火墙应用
</p>

<p align="center">
 <img  src="https://img.shields.io/badge/Jdk-v_1.8-blue?style=flat-square">
 <img  src="https://img.shields.io/badge/SpringBoot-2.7.10-red?style=flat-square">
</p>

# 简介 | x-firewall

    Springboot的前置防火墙应用

* 流量限制
* 访问黑白名单限制
* WFA验证,如OTP,TOTP,微信公众号认证等

# x-firewall交流群

<p>
<img  src="https://img.shields.io/badge/微信-待处理-blue?style=flat-square">
<img  src="https://img.shields.io/badge/QQ-待处理-_?style=flat-square">
</p>


