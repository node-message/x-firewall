package com.nodemessage.x.core;

import com.nodemessage.x.core.totp.exception.MfaTotpException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class LoadProperties {
    public static void load(String referencePackage) {
        Properties prop = new Properties();
        try (InputStream input = MfaTotpException.class.getClassLoader().getResourceAsStream(referencePackage)) {
            prop.load(input);
            prop.forEach((key, value) -> System.setProperty(referencePackage + "." + key.toString(), new String(value.toString().getBytes(StandardCharsets.ISO_8859_1))));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
