package com.nodemessage.x.core.baw;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wjsmc
 * @date 2024/5/16 19:36
 * @description
 **/
@Data
public class BlackAndWhiteDetailed {

    private Serializable flag;

    private BlackAndWhiteType blackAndWhiteType;
}
