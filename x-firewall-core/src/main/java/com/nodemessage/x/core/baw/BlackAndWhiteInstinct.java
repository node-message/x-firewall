package com.nodemessage.x.core.baw;

import java.io.Serializable;

/**
 * @author wjsmc
 * @date 2024/5/16 18:45
 * @description
 **/
public abstract class BlackAndWhiteInstinct {

    protected abstract BlackAndWhiteDetailed queryUser(Serializable flag);

    public abstract void pushUser(BlackAndWhiteDetailed flag);

    public abstract void removeUser(BlackAndWhiteDetailed flag);

    public boolean inboundBlack(Serializable flag) {
        BlackAndWhiteDetailed blackAndWhiteDetailed = queryUser(flag);
        if (blackAndWhiteDetailed == null || !BlackAndWhiteType.Black.equals(blackAndWhiteDetailed.getBlackAndWhiteType())) {
            passVerify(blackAndWhiteDetailed);
            return true;
        }
        denyVerify(blackAndWhiteDetailed);
        return false;
    }

    public boolean inboundWhite(Serializable flag) {
        BlackAndWhiteDetailed blackAndWhiteDetailed = queryUser(flag);
        if (blackAndWhiteDetailed != null) {
            if (BlackAndWhiteType.White.equals(blackAndWhiteDetailed.getBlackAndWhiteType())) {
                passVerify(blackAndWhiteDetailed);

                return true;
            }
        }
        denyVerify(blackAndWhiteDetailed);
        return false;
    }

    protected void passVerify(BlackAndWhiteDetailed blackAndWhiteDetailed) {
    }

    protected void denyVerify(BlackAndWhiteDetailed blackAndWhiteDetailed) {
    }

}
