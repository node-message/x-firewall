package com.nodemessage.x.core.baw;

/**
 * @author wjsmc
 * @date 2024/5/16 18:49
 * @description
 **/
public enum BlackAndWhiteType {
    Black, White
}
