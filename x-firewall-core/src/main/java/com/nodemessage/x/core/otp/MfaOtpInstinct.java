package com.nodemessage.x.core.otp;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import cn.hutool.core.util.StrUtil;
import com.nodemessage.x.core.LoadProperties;
import com.nodemessage.x.core.otp.exception.Keep;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class MfaOtpInstinct {

    static {
        LoadProperties.load(Keep.class.getPackage().getName() + ".properties");
    }

    private final TimedCache<Serializable, String> storeCode;
    private final TimedCache<Serializable, ShearCaptcha> storeCaptcha;

    private final Integer digits;
    private final long timeout;
    private final TimeUnit timeUnit;

    public MfaOtpInstinct(Integer digits, long timeout, TimeUnit timeUnit) {
        this.digits = digits;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        this.storeCode = CacheUtil.newTimedCache(timeUnit.toMillis(timeout));
        this.storeCode.schedulePrune(1000);
        this.storeCaptcha = CacheUtil.newTimedCache(timeUnit.toMillis(timeout));
        this.storeCaptcha.schedulePrune(1000);
    }

    public String randomCode(Serializable deviceId) {
        String generate = new RandomGenerator(digits).generate();
        storeCode.put(deviceId, generate);
        return generate;
    }

    private ShearCaptcha randomCaptcha(Serializable deviceId,
                                       int width, int height,
                                       CodeGenerator codeGenerator) {
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(width, height, digits, 4);
        if (codeGenerator != null) {
            captcha.setGenerator(codeGenerator);
            captcha.createCode();
        }
        storeCaptcha.put(deviceId, captcha);
        return captcha;
    }

    public String randomCaptchaBase64(Serializable deviceId,
                                      int width, int height,
                                      CodeGenerator codeGenerator) {
        ShearCaptcha shearCaptcha = randomCaptcha(deviceId, width, height, codeGenerator);
        return shearCaptcha.getImageBase64Data();
    }

    public byte[] randomCaptchaByte(Serializable deviceId,
                                    int width, int height,
                                    CodeGenerator codeGenerator) {
        ShearCaptcha shearCaptcha = randomCaptcha(deviceId, width, height, codeGenerator);
        return shearCaptcha.getImageBytes();
    }

    public void randomCaptchaWrite(Serializable deviceId,
                                   int width, int height,
                                   CodeGenerator codeGenerator,
                                   OutputStream outputStream) {
        ShearCaptcha shearCaptcha = randomCaptcha(deviceId, width, height, codeGenerator);
        shearCaptcha.write(outputStream);
    }

    public boolean verifyCode(Serializable deviceId, String code) {
        if (StrUtil.equals(storeCode.get(deviceId), code, true)) {
            storeCode.remove(deviceId);
            return true;
        }
        return false;
    }

    public boolean verifyCaptcha(Serializable deviceId, String code) {
        ShearCaptcha shearCaptcha = storeCaptcha.get(deviceId);
        if (shearCaptcha != null && shearCaptcha.verify(code)) {
            storeCaptcha.remove(deviceId);
            return true;
        }
        return false;
    }
}
