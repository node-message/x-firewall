package com.nodemessage.x.core.totp;

import lombok.Data;

import java.io.Serializable;


@Data
public class MfaTotpDevice {
    private Serializable deviceId;
    private String secret;
    private MfaTotpStatus status;
}
