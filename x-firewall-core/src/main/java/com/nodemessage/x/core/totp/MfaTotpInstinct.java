package com.nodemessage.x.core.totp;


import cn.hutool.core.util.StrUtil;
import com.nodemessage.x.core.LoadProperties;
import com.nodemessage.x.core.totp.exception.*;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.code.DefaultCodeVerifier;
import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.exceptions.QrGenerationException;
import dev.samstevens.totp.qr.QrData;
import dev.samstevens.totp.qr.QrDataFactory;
import dev.samstevens.totp.qr.QrGenerator;
import dev.samstevens.totp.qr.ZxingPngQrGenerator;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;
import dev.samstevens.totp.time.SystemTimeProvider;

import java.io.Serializable;

import static dev.samstevens.totp.util.Utils.getDataUriForImage;

public abstract class MfaTotpInstinct {

    static {
        LoadProperties.load(Keep.class.getPackage().getName() + ".properties");
    }

    private final Integer timePeriod;
    private final Integer digits;
    private final HashingAlgorithm algorithm;


    private QrDataFactory qrDataFactory;
    private QrGenerator qrGenerator;
    private DefaultCodeVerifier verifier;
    private final SecretGenerator secretGenerator = new DefaultSecretGenerator();

    public MfaTotpInstinct() {
        this.timePeriod = 30;
        this.digits = 6;
        this.algorithm = HashingAlgorithm.SHA1;
        assemble();
    }

    public MfaTotpInstinct(Integer timePeriod, Integer digits, HashingAlgorithm algorithm) {
        this.timePeriod = timePeriod;
        this.digits = digits;
        this.algorithm = algorithm;
        assemble();
    }

    private void assemble() {
        qrDataFactory = new QrDataFactory(algorithm, digits, timePeriod);
        qrGenerator = new ZxingPngQrGenerator();
        verifier = new DefaultCodeVerifier(new DefaultCodeGenerator(algorithm, digits), new SystemTimeProvider());
        verifier.setTimePeriod(timePeriod);
    }

    abstract protected MfaTotpDevice queryDevice(Serializable deviceId);

    abstract protected void bindDevice(Serializable deviceId, String secret, boolean isVerifyOk);

    abstract protected void unbindDevice(Serializable deviceId);

    public MfaTotpQrInfo registerDevice(Serializable deviceId, String label, String issuer) {
        String secret = secretGenerator.generate();
        QrData data = qrDataFactory.newBuilder().label(label).secret(secret).issuer(issuer).build();
        String qrCodeImage;
        try {
            qrCodeImage = getDataUriForImage(
                    qrGenerator.generate(data),
                    qrGenerator.getImageMimeType());
        } catch (QrGenerationException e) {
            throw new MfaTotpQrCodeException(e);
        }
        bindDevice(deviceId, secret, false);
        return new MfaTotpQrInfo(secret, qrCodeImage);
    }

    public boolean registerVerifyDevice(Serializable deviceId, String verifyCode) {
        if (checkCode(deviceId, verifyCode, true)) {
            bindDevice(deviceId, null, true);
            return true;
        }
        return false;
    }

    public void cancelDevice(Serializable deviceId, String verifyCode) {
        if (checkCode(deviceId, verifyCode, false)) {
            throw new MfaTotpVerifyCodeException();
        }
        unbindDevice(deviceId);
    }


    private boolean checkCode(Serializable uid, String verifyCode, boolean ignoreStatus) {
        MfaTotpDevice device = queryDevice(uid);
        if (device == null) {
            throw new MfaTotpNotFoundException();
        }
        if (StrUtil.isEmpty(device.getSecret())) {
            return false;
        }
        if (!ignoreStatus && MfaTotpStatus.UNAUTHORIZED.equals(device.getStatus())) {
            throw new MfaTotpUnauthorizedException();
        }
        return verifier.isValidCode(device.getSecret(), verifyCode);
    }

    public boolean checkCode(Serializable uid, String verifyCode) {
        return checkCode(uid, verifyCode, false);
    }

}

