package com.nodemessage.x.core.totp;


public class MfaTotpQrInfo {
    private final String secret;
    private final String qrCodeImage;

    public MfaTotpQrInfo(String secret, String qrCodeImage) {
        this.secret = secret;
        this.qrCodeImage = qrCodeImage;
    }

    public String getSecret() {
        return secret;
    }

    public String getQrCodeImage() {
        return qrCodeImage;
    }

    @Override
    public String toString() {
        return "MfaTotpQrInfo{" +
                "secret='" + secret + '\'' +
                ", qrCodeImage='" + qrCodeImage + '\'' +
                '}';
    }
}
