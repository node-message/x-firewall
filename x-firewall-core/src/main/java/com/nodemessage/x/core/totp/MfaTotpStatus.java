package com.nodemessage.x.core.totp;

public enum MfaTotpStatus {
    NORMAL, UNAUTHORIZED
}
