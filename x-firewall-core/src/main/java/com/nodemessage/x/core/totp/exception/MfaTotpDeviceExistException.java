package com.nodemessage.x.core.totp.exception;

public class MfaTotpDeviceExistException extends MfaTotpException {
    public MfaTotpDeviceExistException() {
        super(System.getProperty(MfaTotpDeviceExistException.class.getName()));
    }
}
