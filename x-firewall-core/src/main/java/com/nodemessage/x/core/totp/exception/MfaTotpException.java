package com.nodemessage.x.core.totp.exception;

public class MfaTotpException extends RuntimeException {

    public MfaTotpException() {
    }

    public MfaTotpException(String message) {
        super(message);
    }

    public MfaTotpException(String message, Throwable cause) {
        super(message, cause);
    }

    public MfaTotpException(Throwable cause) {
        super(cause);
    }

    public MfaTotpException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
