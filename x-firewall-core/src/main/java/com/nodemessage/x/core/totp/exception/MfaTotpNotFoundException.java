package com.nodemessage.x.core.totp.exception;

public class MfaTotpNotFoundException extends MfaTotpException {
    public MfaTotpNotFoundException() {
        super(System.getProperty(MfaTotpNotFoundException.class.getName()));
    }
}
