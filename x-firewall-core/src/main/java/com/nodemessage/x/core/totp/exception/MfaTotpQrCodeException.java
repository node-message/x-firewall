package com.nodemessage.x.core.totp.exception;

public class MfaTotpQrCodeException extends MfaTotpException {
    public MfaTotpQrCodeException() {
        super(System.getProperty(MfaTotpQrCodeException.class.getName()));
    }

    public MfaTotpQrCodeException(Throwable cause) {
        super(System.getProperty(MfaTotpQrCodeException.class.getName()), cause);
    }
}
