package com.nodemessage.x.core.totp.exception;

public class MfaTotpUnauthorizedException extends MfaTotpException {
    public MfaTotpUnauthorizedException() {
        super(System.getProperty(MfaTotpUnauthorizedException.class.getName()));
    }
}
