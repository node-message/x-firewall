package com.nodemessage.x.core.totp.exception;

public class MfaTotpVerifyCodeException extends MfaTotpException {
    public MfaTotpVerifyCodeException() {
        super(System.getProperty(MfaTotpVerifyCodeException.class.getName()));
    }
}
