package com.nodemessage.x.plugin;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public abstract class BasePlugin {

    private final ConcurrentHashMap<Class<?>, Set<Class<?>>> interfaceStore = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Class<?>> classStore = new ConcurrentHashMap<>();

    public abstract void register();

    protected Set<Class<?>> getInterface(Class<?> clazz) {
        Set<Class<?>> classes = interfaceStore.get(clazz);
        if (classes != null) {
            return classes;
        } else {
            return Collections.emptySet();
        }
    }

    protected synchronized void putInterfaceImpl(Class<?> interfaceClazz, Class<?> implClazz) {
        Set<Class<?>> classes = interfaceStore.computeIfAbsent(interfaceClazz, k -> new HashSet<>());
        classes.add(implClazz);
    }

    protected Class<?> getClass(String classReference) {
        return classStore.get(classReference);
    }

    protected synchronized void putClass(Class<?> clazz) {
        classStore.put(clazz.getCanonicalName(), clazz);
    }

    protected ConcurrentHashMap<Class<?>, Set<Class<?>>> getInterfaceStore() {
        return interfaceStore;
    }

    protected ConcurrentHashMap<String, Class<?>> getClassStore() {
        return classStore;
    }
}
