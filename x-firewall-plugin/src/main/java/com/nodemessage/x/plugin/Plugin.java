package com.nodemessage.x.plugin;


import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class Plugin {
    /**
     * 插件路径
     * */
    @NonNull
    private String path;
    /**
     * 插件类型
     * */
    @NonNull
    private PluginType pluginType;
}
