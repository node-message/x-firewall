package com.nodemessage.x.plugin;

import sun.misc.ClassLoaderUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 热拔插插件实例
 */
public class PluginInstinct {

    /**
     * jar监听路径
     */
    private URLClassLoader urlClassLoader;

    /**
     * 拓展的接口package名
     */
    private final List<String> basePkg = new ArrayList<>();

    private final ConcurrentHashMap<Class<?>, Set<Class<?>>> interfaceStore = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Set<Class<?>>> classStore = new ConcurrentHashMap<>();
    private long refreshTime = 0;

    public PluginInstinct(String path) {
        try (Stream<Path> walk = Files.walk(Paths.get(path))) {
            List<Path> pathsList = walk.filter(Files::isRegularFile)
                    .filter(e -> e.toString().toLowerCase().endsWith(".jar")).collect(Collectors.toList());
            urlClassLoader = new URLClassLoader(pathsList.stream().map(e -> {
                try {
                    return e.toUri().toURL();
                } catch (MalformedURLException ex) {
                    throw new RuntimeException(ex);
                }
            }).toArray(URL[]::new));
            basePkg.addAll(pathsList.stream()
                    .map(e -> parsePackageByJarPath(e.toString()))
                    .collect(Collectors.toList()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 加载插件类信息
     */
    private synchronized void InitInstance() {
        try {
            /* 清空原始信息 */
            interfaceStore.clear();
            classStore.clear();
            for (String pkg : basePkg) {
                /* 加载插件启动器 */
                BasePlugin pluginBootstrap = (BasePlugin) urlClassLoader
                        .loadClass(pkg + ".PluginBootstrap").newInstance();
                pluginBootstrap.register();
                /* 加载接口信息 */
                ConcurrentHashMap<Class<?>, Set<Class<?>>> interfaceStore = pluginBootstrap.getInterfaceStore();
                for (Map.Entry<Class<?>, Set<Class<?>>> classSetEntry : interfaceStore.entrySet()) {
                    Class<?> key = classSetEntry.getKey();
                    Set<Class<?>> value = classSetEntry.getValue();
                    List<String> valueReference = value.stream().map(Class::getCanonicalName).collect(Collectors.toList());
                    Set<Class<?>> classes = this.interfaceStore.computeIfAbsent(key, e -> new HashSet<>());
                    /* 校验是否重复 */
                    List<Class<?>> existClass = classes.stream()
                            .filter(e -> valueReference.contains(e.getCanonicalName())).collect(Collectors.toList());
                    if (existClass.size() > 0) {
                        throw new PluginRegisterException("Plugin interface " + key.getCanonicalName() + "存在多个相同引用实现:" +
                                existClass.stream().map(Class::getCanonicalName).distinct().collect(Collectors.joining(",")));
                    } else {
                        classes.addAll(value);
                    }
                }
                /* 加载类信息 */
                ConcurrentHashMap<String, Class<?>> classStore = pluginBootstrap.getClassStore();
                for (Map.Entry<String, Class<?>> classEntry : classStore.entrySet()) {
                    String key = classEntry.getKey();
                    Class<?> value = classEntry.getValue();
                    Set<Class<?>> classes = this.classStore.computeIfAbsent(key, e -> new HashSet<>());
                    if (classes.stream().anyMatch(e -> value.getCanonicalName().equals(e.getCanonicalName()))) {
                        throw new PluginRegisterException("Plugin class " + key + "存在多个相同引用实现:" + value.getCanonicalName());
                    } else {
                        classes.add(value);
                    }
                }
            }
            ClassLoaderUtil.releaseLoader(urlClassLoader);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 根据jar的文件名解析 package reference
     */
    private String parsePackageByJarPath(String jarPath) {
        return Paths.get(jarPath).getFileName().toString()
                .replaceAll("\\.[jJ][aA][rR]", "");
    }


    /**
     * 监听插件位置变化
     */
    private void watchLibChanged(String path) {
        ExecutorService executorService = Executors.newSingleThreadExecutor(r -> {
            final Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.execute(() -> {
            ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(r -> {
                final Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            });
            ScheduledFuture<?> schedule = null;
            try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
                Path watchPath;
                boolean fileFlag = false;
                Path basePath = Paths.get(path);
                if (path.toLowerCase().endsWith(".jar")) {
                    watchPath = basePath.getParent();
                    fileFlag = true;
                } else {
                    watchPath = basePath;
                }
                watchPath.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY
                        , StandardWatchEventKinds.ENTRY_CREATE
                        , StandardWatchEventKinds.ENTRY_DELETE);
                while (true) {
                    WatchKey take = watchService.take();
                    for (WatchEvent<?> event : take.pollEvents()) {
                        Path fileName = (Path) event.context();
                        WatchEvent.Kind<?> kind = event.kind();
                        try {
                            if (fileFlag) {
                                Path changeFile = fileName.getFileName();
                                if (!changeFile.toString().equalsIgnoreCase(basePath.getFileName().toString())) {
                                    continue;
                                }
                            }
                            if (kind.equals(StandardWatchEventKinds.OVERFLOW)) {
                                continue;
                            }
                            /* 延时更新 */
                            if (schedule != null) {
                                if ((System.currentTimeMillis() - refreshTime) / 1000 <= 5) {
                                    schedule.cancel(true);
                                }
                            }
                            schedule = scheduledExecutorService
                                    .schedule(this::InitInstance, 5, TimeUnit.SECONDS);
                            refreshTime = System.currentTimeMillis();
                        } finally {
                            take.reset();
                        }
                    }
                }
            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                scheduledExecutorService.shutdown();
                executorService.shutdown();
            }
        });
    }


    /**
     * 加载插件文件位置
     */
    public static PluginInstinct Load(Plugin plugin) {
        PluginInstinct pluginInstinct = new PluginInstinct(plugin.getPath());
        switch (plugin.getPluginType()) {
            case DIRECTORY:
                try (Stream<Path> pathsStream = Files.walk(Paths.get(plugin.getPath()))) {
                    List<Path> pathsList = pathsStream.filter(Files::isRegularFile)
                            .filter(e -> e.toString().toLowerCase().endsWith(".jar")).collect(Collectors.toList());
                    pluginInstinct.urlClassLoader = new URLClassLoader(pathsList.stream().map(e -> {
                        try {
                            return e.toUri().toURL();
                        } catch (MalformedURLException ex) {
                            throw new RuntimeException(ex);
                        }
                    }).toArray(URL[]::new));
                    pluginInstinct.basePkg.addAll(pathsList.stream()
                            .map(e -> pluginInstinct.parsePackageByJarPath(e.toString()))
                            .collect(Collectors.toList()));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                break;
            case JAR:
                try {
                    pluginInstinct.urlClassLoader = new URLClassLoader(new URL[]{new File(plugin.getPath()).toURI().toURL()});
                    pluginInstinct.basePkg.add(pluginInstinct.parsePackageByJarPath(plugin.getPath()));
                } catch (MalformedURLException e) {
                    throw new RuntimeException(e);
                }
                break;
            default:
                throw new PluginTypeException("插件类型不正确");
        }
        pluginInstinct.InitInstance();
        pluginInstinct.watchLibChanged(plugin.getPath());
        return pluginInstinct;
    }

    /**
     * 反射执行对象方法,有返回值
     */
    @SuppressWarnings("unchecked")
    public static <T> T execMethod(Object obj, String method, Object... args) {
        Class<?> clazz = obj.getClass();
        try {
            Method declaredMethod = clazz.getDeclaredMethod(method, Arrays.stream(args).map(Object::getClass).toArray(Class[]::new));
            declaredMethod.setAccessible(true);
            return (T) declaredMethod.invoke(obj, args);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 反射执行对象方法,无返回值
     */
    public static void execMethodVoid(Object obj, String method, Object... args) {
        Class<?> clazz = obj.getClass();
        try {
            Method declaredMethod = clazz.getDeclaredMethod(method, Arrays.stream(args).map(Object::getClass).toArray(Class[]::new));
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, args);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
