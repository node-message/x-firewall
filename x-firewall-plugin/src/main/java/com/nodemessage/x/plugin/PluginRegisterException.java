package com.nodemessage.x.plugin;

public class PluginRegisterException extends RuntimeException {
    public PluginRegisterException() {
        super();
    }

    public PluginRegisterException(String message) {
        super(message);
    }

    public PluginRegisterException(String message, Throwable cause) {
        super(message, cause);
    }
}
