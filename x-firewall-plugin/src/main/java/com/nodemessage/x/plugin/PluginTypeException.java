package com.nodemessage.x.plugin;

public class PluginTypeException extends RuntimeException{
    public PluginTypeException(String message) {
        super(message);
    }
}
