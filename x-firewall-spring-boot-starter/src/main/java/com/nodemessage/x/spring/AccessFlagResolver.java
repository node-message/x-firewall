package com.nodemessage.x.spring;


import cn.hutool.extra.servlet.ServletUtil;

import javax.servlet.http.HttpServletRequest;

@FunctionalInterface
public interface AccessFlagResolver {
    String resolverFlag(HttpServletRequest request);


    class IpaddrResolver implements AccessFlagResolver {
        @Override
        public String resolverFlag(HttpServletRequest request) {
            return ServletUtil.getClientIP(request);
        }
    }

}
