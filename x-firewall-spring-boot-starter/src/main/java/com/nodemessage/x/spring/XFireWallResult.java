package com.nodemessage.x.spring;


import lombok.Data;

@Data
public class XFireWallResult {

    public static final Integer StatusSuccess = 200;
    public static final Integer StatusError = 500;
    public static final Integer StatusNoAuth = 403;
    public static final Integer StatusMismatchingCode = 4000;

    private Integer code;
    private String msg;
    private Object data;

    public XFireWallResult(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static XFireWallResult success(String msg, Object data) {
        return new XFireWallResult(StatusSuccess, msg, data);
    }

    public static XFireWallResult success(String msg) {
        return success(msg, null);
    }

    public static XFireWallResult success(Object data) {
        return success("成功", data);
    }

    public static XFireWallResult success() {
        return success("成功", null);
    }

    public static XFireWallResult error(String msg, Object data) {
        return new XFireWallResult(StatusError, msg, data);
    }

    public static XFireWallResult error(String msg) {
        return error(msg, null);
    }

    public static XFireWallResult error(Integer status, String msg) {
        return new XFireWallResult(status, msg, null);
    }

}
