package com.nodemessage.x.spring;

import com.nodemessage.x.spring.config.AccessConfig;
import com.nodemessage.x.spring.config.ContentAnalyseConfig;
import com.nodemessage.x.spring.config.DDoSConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wjsmc
 * @date 2024/5/16 11:10
 * @description
 **/
public interface XFirewallFilterConfiguration {

    default void configCors(CorsRegistry corsRegistry) {
    }

    default void configAccess(AccessConfig accessConfig) {
    }

    default void configContentAnalyse(ContentAnalyseConfig contentAnalyseConfig) {
    }

    default void configDDoSFilter(DDoSConfig dDoSConfig) {
    }

    @ConditionalOnMissingBean(XFirewallFilterConfiguration.class)
    class DefaultXFirewallFilterConfiguration implements XFirewallFilterConfiguration {
        @Override
        public void configCors(CorsRegistry corsRegistry) {
            corsRegistry.addMapping("/**")
                    .allowedOriginPatterns("*")
                    .allowCredentials(true)
                    .allowedMethods("GET", "POST", "PUT", "DELETE")
                    .allowedHeaders("*")
                    .exposedHeaders("*");
        }
    }
}
