package com.nodemessage.x.spring;

import cn.hutool.core.util.ObjectUtil;
import com.nodemessage.x.core.baw.BlackAndWhiteInstinct;
import com.nodemessage.x.spring.config.AccessConfig;
import com.nodemessage.x.spring.config.ContentAnalyseConfig;
import com.nodemessage.x.spring.config.DDoSConfig;
import com.nodemessage.x.spring.filter.AccessFilter;
import com.nodemessage.x.spring.filter.ContentAnalyseFilter;
import com.nodemessage.x.spring.filter.DDoSFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author wjsmc
 * @date 2024/5/16 11:13
 * @description
 **/
@Slf4j
public class XFirewallFilterRegistrar implements WebMvcConfigurer {

    public static final Integer FilterStartOrder = 0;

    @Autowired
    private XFirewallFilterConfiguration xFirewallFilterConfiguration;
    @Autowired
    private XFirewallProperties xFirewallProperties;
    @Autowired
    private BlackAndWhiteInstinct blackAndWhiteInstinct;
    @Autowired
    private ServerProperties serverProperties;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        if (xFirewallProperties.getFilter().getCors()) {
            xFirewallFilterConfiguration.configCors(registry);
            log.info("[ x-firewall ] cors loading.");
        }
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/x-firewall/**")
                .addResourceLocations("classpath:/x-firewall/");
        log.info("[ x-firewall ] add resource visit: {}/x-firewall/index.html", ObjectUtil.defaultIfEmpty(serverProperties.getServlet().getContextPath(), ""));
    }

    /**
     * Ddos攻击,流量限制等
     */
    @Bean
    @ConditionalOnProperty(value = "x-firewall.filter.ddos", havingValue = "true")
    public FilterRegistrationBean<DDoSFilter> doSFilterFilterRegistrationBean() {
        FilterRegistrationBean<DDoSFilter> bean = new FilterRegistrationBean<>();
        DDoSConfig dDoSConfig = new DDoSConfig();
        xFirewallFilterConfiguration.configDDoSFilter(dDoSConfig);
        DDoSFilter dDoSFilter = new DDoSFilter(dDoSConfig);
        bean.setFilter(dDoSFilter);
        bean.setName(XFirewallFilters.DDoSFilter.name);
        bean.setOrder(XFirewallFilters.DDoSFilter.order);
        bean.addUrlPatterns("/*");
        return bean;
    }

    /**
     * 访问拦截器,黑白名单
     */
    @Bean
    @ConditionalOnProperty(value = "x-firewall.filter.access", havingValue = "true")
    public FilterRegistrationBean<AccessFilter> accessFilterFilterRegistrationBean() {
        FilterRegistrationBean<AccessFilter> bean = new FilterRegistrationBean<>();
        AccessConfig accessConfig = new AccessConfig();
        xFirewallFilterConfiguration.configAccess(accessConfig);
        AccessFilter accessFilter = new AccessFilter(accessConfig, blackAndWhiteInstinct);
        bean.setFilter(accessFilter);
        bean.setName(XFirewallFilters.AccessFilter.name);
        bean.setOrder(XFirewallFilters.AccessFilter.order);
        bean.addUrlPatterns("/*");
        return bean;
    }

    /**
     * 请求内容拦截,SQL注入,XSS跨站等
     */
    @Bean
    @ConditionalOnProperty(value = "x-firewall.filter.contentAnalyse", havingValue = "true")
    public FilterRegistrationBean<ContentAnalyseFilter> contentAnalyseFilterFilterRegistrationBean() {
        FilterRegistrationBean<ContentAnalyseFilter> bean = new FilterRegistrationBean<>();
        ContentAnalyseConfig contentAnalyseConfig = new ContentAnalyseConfig();
        xFirewallFilterConfiguration.configContentAnalyse(contentAnalyseConfig);
        ContentAnalyseFilter contentAnalyseFilter = new ContentAnalyseFilter(contentAnalyseConfig);
        bean.setFilter(contentAnalyseFilter);
        bean.setName(XFirewallFilters.ContentAnalyseFilter.name);
        bean.setOrder(XFirewallFilters.ContentAnalyseFilter.order);
        bean.addUrlPatterns("/*");
        return bean;
    }

}
