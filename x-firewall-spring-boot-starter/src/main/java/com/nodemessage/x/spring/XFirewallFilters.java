package com.nodemessage.x.spring;

import static com.nodemessage.x.spring.XFirewallFilterRegistrar.FilterStartOrder;

/**
 * @author wjsmc
 * @date 2024/5/16 16:49
 * @description
 **/
public enum XFirewallFilters {

    DDoSFilter("dDoSFilter", FilterStartOrder),

    ContentAnalyseFilter("contentAnalyseFilter", FilterStartOrder + 1),

    AccessFilter("accessFilter", FilterStartOrder + 2);


    public final String name;
    public final Integer order;

    XFirewallFilters(String name, Integer order) {
        this.name = name;
        this.order = order;
    }
}
