package com.nodemessage.x.spring;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author wjsmc
 * @date 2024/5/16 11:05
 * @description
 **/
@ConfigurationProperties("x-firewall")
@EnableConfigurationProperties
@Data
public class XFirewallProperties {

    private Filter filter = new Filter();
    private String username = "xfirewall";
    private String password = "xfirewall";

    @ConfigurationProperties("x-firewall.filter")
    @Data
    public static class Filter {
        private Boolean cors = false;
        private Boolean access = false;
        private Boolean ddos = false;
        private Boolean contentAnalyse = false;
    }

}
