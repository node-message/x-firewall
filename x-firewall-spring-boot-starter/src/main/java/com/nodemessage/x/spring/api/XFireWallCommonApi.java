package com.nodemessage.x.spring.api;


import com.nodemessage.x.core.otp.MfaOtpInstinct;
import com.nodemessage.x.spring.XFireWallResult;
import com.nodemessage.x.spring.XFirewallProperties;
import com.nodemessage.x.spring.consts.ApiConst;
import com.nodemessage.x.spring.entity.UserLoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/x-firewall")
public class XFireWallCommonApi {

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private XFirewallProperties xFirewallProperties;

    private final MfaOtpInstinct mfaOtpInstinct = new MfaOtpInstinct(4, 60, TimeUnit.SECONDS);

    @PostMapping("login")
    public XFireWallResult login(@RequestBody UserLoginDto loginDto) {
        if (xFirewallProperties.getUserName().equals(loginDto.getUsername()) &&
                mfaOtpInstinct.verifyCaptcha(httpSession.getId(), loginDto.getCode()) &&
                xFirewallProperties.getPassword().equals(loginDto.getPassword())) {
            return XFireWallResult.success();
        } else {
            return XFireWallResult.error(XFireWallResult.StatusMismatchingCode, "验证码不正确");
        }
    }

    @GetMapping("code")
    public void code(HttpServletResponse response) throws IOException {
        String id = httpSession.getId();
        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        mfaOtpInstinct.randomCaptchaWrite(id, 120, 40, null, outputStream);
    }

    @GetMapping("logout")
    public XFireWallResult logout() {
        httpSession.removeAttribute(ApiConst.AuthName);
        return XFireWallResult.success();
    }
}
