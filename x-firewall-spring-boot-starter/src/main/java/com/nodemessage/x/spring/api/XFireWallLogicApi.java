package com.nodemessage.x.spring.api;


import com.nodemessage.x.spring.XFireWallResult;
import com.nodemessage.x.spring.consts.ApiConst;
import com.nodemessage.x.spring.exception.NoAuthException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Collections;

@RestController
@RequestMapping("/x-firewall")
@Slf4j
public class XFireWallLogicApi {

    @Autowired
    private HttpSession httpSession;

    @ModelAttribute
    public void before() {
        if (httpSession.getAttribute(ApiConst.AuthName) == null) {
            throw new NoAuthException("请认证后操作");
        }
    }


    @GetMapping("visit")
    public XFireWallResult visit() {
        return XFireWallResult.success(Collections.emptySet());
    }


}
