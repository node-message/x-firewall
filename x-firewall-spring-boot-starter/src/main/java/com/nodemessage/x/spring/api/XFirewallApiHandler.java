package com.nodemessage.x.spring.api;

import com.nodemessage.x.spring.XFireWallResult;
import com.nodemessage.x.spring.exception.NoAuthException;
import com.nodemessage.x.spring.exception.XFirewallCommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author wjsmc
 * @date 2024/5/18 9:38
 * @description
 **/

@RestControllerAdvice
@RestController
@Slf4j
public class XFirewallApiHandler {

    @ExceptionHandler(XFirewallCommonException.class)
    public XFireWallResult xFirewallCommonException(XFirewallCommonException e) {
        log.error("[ x-firewall ] 异常", e);
        return XFireWallResult.error(XFireWallResult.StatusError, e.getMessage());
    }

    @ExceptionHandler(NoAuthException.class)
    public XFireWallResult noAuthException(NoAuthException e) {
        log.error("[ x-firewall ] 未登录");
        return XFireWallResult.error(XFireWallResult.StatusNoAuth, e.getMessage());
    }

}
