package com.nodemessage.x.spring.config;

import com.nodemessage.x.spring.AccessFlagResolver;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.Set;

/**
 * @author wjsmc
 * @date 2024/5/16 15:04
 * @description
 **/
public class AccessConfig {
    @Getter
    private final Set<String> whitePattern = new HashSet<>();
    @Getter
    private final Set<String> blackPattern = new HashSet<>();

    @Getter
    @Setter
    @Accessors(chain = true)
    private AccessFlagResolver accessFlagResolver = new AccessFlagResolver.IpaddrResolver();

    public AccessConfig addWhitePattern(String pattern) {
        whitePattern.add(pattern);
        return this;
    }

    public AccessConfig addBlackPattern(String pattern) {
        blackPattern.add(pattern);
        return this;
    }

    public AccessConfig removeWhitePattern(String pattern) {
        whitePattern.remove(pattern);
        return this;
    }

    public AccessConfig removeBlackPattern(String pattern) {
        blackPattern.remove(pattern);
        return this;
    }
}
