package com.nodemessage.x.spring.db;

import lombok.Data;

import java.io.Serializable;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * @author wjsmc
 * @date 2024/5/18 16:57
 * @description
 **/
public class MapXFireWallStore<V extends MapXFireWallStore.Value> extends XFireWallStore<V> {

    private final ConcurrentNavigableMap<Serializable, V> store;

    public MapXFireWallStore(XFireWallDb xFireWallDb, String spaceName) {
        super(xFireWallDb);
        store = (ConcurrentNavigableMap<Serializable, V>) xFireWallDb.getDb().treeMap(spaceName)
                .createOrOpen();
    }

    @Override
    public V select(Serializable key) {
        return store.get(key);
    }

    @Override
    protected V insertLogic(V entity) {
        return store.putIfAbsent(entity.getKey(), entity);
    }

    @Override
    protected V updateLogic(V entity) {
        return store.replace(entity.getKey(), entity);
    }

    @Override
    protected boolean deleteLogic(Serializable key) {
        if (store.get(key) == null) {
            return false;
        }
        return store.remove(key) != null;
    }


    @Data
    public static class Value implements Serializable {
        private Serializable key;
    }
}
