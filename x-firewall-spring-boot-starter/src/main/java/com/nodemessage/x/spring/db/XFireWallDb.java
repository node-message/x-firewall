package com.nodemessage.x.spring.db;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author wjsmc
 * @date 2024/5/18 15:49
 * @description
 **/
public class XFireWallDb {
    private DB db;
    private Path dbFilePath = Paths.get(System.getProperty("user.home"), "x-firewall", "x-firewall.db");

    public XFireWallDb() {
        try {
            if (!Files.exists(dbFilePath.getParent())) {
                Files.createDirectories(dbFilePath.getParent());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        db = DBMaker.fileDB(dbFilePath.toString())
                .closeOnJvmShutdown()
                .make();
    }

    public DB getDb() {
        return db;
    }

    public void commit() {
        db.commit();
    }

    public void rollback() {
        db.rollback();
    }

    public void close() {
        db.close();
    }
}
