package com.nodemessage.x.spring.db;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author wjsmc
 * @date 2024/5/18 16:54
 * @description
 **/
public abstract class XFireWallStore<V> {

    protected final XFireWallDb xFireWallDb;

    public XFireWallStore(XFireWallDb xFireWallDb) {
        this.xFireWallDb = xFireWallDb;
    }

    public abstract V select(Serializable kek);

    public V insert(V entity) {
        V result = insertLogic(entity);
        if (result == null) {
            this.xFireWallDb.commit();
            return entity;
        } else {
            return null;
        }
    }

    protected abstract V insertLogic(V entity);

    public V update(V entity) {
        V result = updateLogic(entity);
        if (result != null) {
            this.xFireWallDb.commit();
            return entity;
        }
        return null;
    }

    protected abstract V updateLogic(V entity);


    public boolean delete(Serializable key) {
        boolean result = deleteLogic(key);
        this.xFireWallDb.commit();
        return result;
    }

    protected abstract boolean deleteLogic(Serializable key);

}
