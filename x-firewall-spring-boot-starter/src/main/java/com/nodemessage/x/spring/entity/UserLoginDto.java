package com.nodemessage.x.spring.entity;

import lombok.Data;

@Data
public class UserLoginDto {
    private String username;
    private String password;
    private String code;
}
