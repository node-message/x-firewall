package com.nodemessage.x.spring.exception;

/**
 * @author wjsmc
 * @date 2024/5/18 9:37
 * @description
 **/
public class NoAuthException extends RuntimeException {
    public NoAuthException(String message) {
        super(message);
    }
}
