package com.nodemessage.x.spring.exception;

/**
 * @author wjsmc
 * @date 2024/5/18 9:37
 * @description
 **/
public class XFirewallCommonException extends RuntimeException {
    public XFirewallCommonException() {
    }

    public XFirewallCommonException(String message) {
        super(message);
    }

    public XFirewallCommonException(String message, Throwable cause) {
        super(message, cause);
    }

    public XFirewallCommonException(Throwable cause) {
        super(cause);
    }

    public XFirewallCommonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
