package com.nodemessage.x.spring.filter;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.nodemessage.x.core.baw.BlackAndWhiteInstinct;
import com.nodemessage.x.spring.config.AccessConfig;
import org.springframework.http.server.PathContainer;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.pattern.PathPatternParser;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wjsmc
 * @date 2024/5/16 12:23
 * @description
 **/
public class AccessFilter extends OncePerRequestFilter {

    private final AccessConfig accessConfig;

    private final BlackAndWhiteInstinct blackAndWhiteInstinct;

    public AccessFilter(AccessConfig accessConfig, BlackAndWhiteInstinct blackAndWhiteInstinct) {
        this.accessConfig = accessConfig;
        this.blackAndWhiteInstinct = blackAndWhiteInstinct;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        PathContainer pathContainer = PathContainer.parsePath(request.getServletPath());
        String xFlag = accessConfig.getAccessFlagResolver().resolverFlag(request);
        if (StrUtil.isBlank(xFlag)) {
            return;
        }
        for (String b : accessConfig.getBlackPattern()) {
            if (PathPatternParser.defaultInstance.parse(b).matches(pathContainer)) {
                if (blackAndWhiteInstinct.inboundBlack(xFlag)) {
                    filterChain.doFilter(request, response);
                }
                return;
            }
        }
        for (String w : accessConfig.getWhitePattern()) {
            if (PathPatternParser.defaultInstance.parse(w).matches(pathContainer)) {
                if (blackAndWhiteInstinct.inboundWhite(xFlag)) {
                    filterChain.doFilter(request, response);
                }
                return;
            }
        }
        filterChain.doFilter(request, response);
    }


}
