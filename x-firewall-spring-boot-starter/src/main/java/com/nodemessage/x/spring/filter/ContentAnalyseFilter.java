package com.nodemessage.x.spring.filter;

import com.nodemessage.x.spring.config.ContentAnalyseConfig;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wjsmc
 * @date 2024/5/16 16:04
 * @description
 **/
public class ContentAnalyseFilter extends OncePerRequestFilter {

    public final ContentAnalyseConfig contentAnalyseConfig;

    public ContentAnalyseFilter(ContentAnalyseConfig contentAnalyseConfig) {
        this.contentAnalyseConfig = contentAnalyseConfig;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        filterChain.doFilter(request, response);
    }
}
