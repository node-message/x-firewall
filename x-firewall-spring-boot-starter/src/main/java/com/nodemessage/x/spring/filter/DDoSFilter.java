package com.nodemessage.x.spring.filter;

import com.nodemessage.x.spring.config.DDoSConfig;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wjsmc
 * @date 2024/5/16 16:12
 * @description
 **/
public class DDoSFilter extends OncePerRequestFilter {
    private final DDoSConfig dDoSConfig;

    public DDoSFilter(DDoSConfig dDoSConfig) {
        this.dDoSConfig = dDoSConfig;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        filterChain.doFilter(request, response);
    }
}
