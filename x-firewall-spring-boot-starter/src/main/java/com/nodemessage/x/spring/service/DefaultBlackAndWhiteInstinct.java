package com.nodemessage.x.spring.service;

import cn.hutool.json.JSONUtil;
import com.nodemessage.x.core.baw.BlackAndWhiteDetailed;
import com.nodemessage.x.core.baw.BlackAndWhiteInstinct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wjsmc
 * @date 2024/5/16 19:05
 * @description
 **/
@ConditionalOnMissingBean(BlackAndWhiteInstinct.class)
@Slf4j
public class DefaultBlackAndWhiteInstinct extends BlackAndWhiteInstinct {

    public final ConcurrentHashMap<Serializable, BlackAndWhiteDetailed> store = new ConcurrentHashMap<>(20);

    @Override
    protected BlackAndWhiteDetailed queryUser(Serializable flag) {
        return store.get(flag);
    }

    @Override
    public void pushUser(BlackAndWhiteDetailed flag) {
        store.put(flag.getFlag(), flag);
    }

    @Override
    public void removeUser(BlackAndWhiteDetailed flag) {
        store.remove(flag.getFlag(), flag);
    }

    @Override
    protected void denyVerify(BlackAndWhiteDetailed blackAndWhiteDetailed) {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        if (response == null) {
            return;
        }
        try {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            response.getWriter().println(JSONUtil.toJsonStr(Collections.singletonMap("msg", "非法访问")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
