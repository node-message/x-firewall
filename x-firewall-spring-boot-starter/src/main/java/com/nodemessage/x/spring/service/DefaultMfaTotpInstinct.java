package com.nodemessage.x.spring.service;

import com.nodemessage.x.core.baw.BlackAndWhiteInstinct;
import com.nodemessage.x.core.totp.MfaTotpDevice;
import com.nodemessage.x.core.totp.MfaTotpInstinct;
import com.nodemessage.x.core.totp.MfaTotpStatus;
import com.nodemessage.x.core.totp.exception.MfaTotpDeviceExistException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wjsmc
 * @date 2024/5/17 9:45
 * @description
 **/
@ConditionalOnMissingBean(MfaTotpInstinct.class)
@Slf4j
public class DefaultMfaTotpInstinct extends MfaTotpInstinct {
    private final ConcurrentHashMap<Serializable, MfaTotpDevice> store = new ConcurrentHashMap<>();

    @Override
    protected MfaTotpDevice queryDevice(Serializable deviceId) {
        return store.get(deviceId);
    }

    @Override
    protected void bindDevice(Serializable deviceId, String secret, boolean isVerifyOk) {
        MfaTotpDevice device = store.get(deviceId);
        if (isVerifyOk) {
            if (device != null && MfaTotpStatus.UNAUTHORIZED.equals(device.getStatus())) {
                device.setStatus(MfaTotpStatus.NORMAL);
            }
        } else {
            if (device != null) {
                throw new MfaTotpDeviceExistException();
            }
            device = new MfaTotpDevice();
            device.setDeviceId(deviceId);
            device.setSecret(secret);
            device.setStatus(MfaTotpStatus.UNAUTHORIZED);
            store.put(deviceId, device);
        }
    }

    @Override
    protected void unbindDevice(Serializable deviceId) {
        store.remove(deviceId);
    }
}
