function queryLocalBaseUrl() {
    let pathSplit = location.pathname.split("/x-firewall");
    return pathSplit[0] + "/x-firewall"
}

(function () {
    let passUrl = ["/x-firewall/login.html"]
    for (let url of passUrl) {
        if (location.pathname.endsWith(url)) {
            return
        }
    }
    if (localStorage.getItem("x-firewall") == null) {
        location.href = queryLocalBaseUrl() + "/login.html"
    }
})()

layui.extend({
    api: 'lib/api',
    properties: 'properties',
}).define('properties', function (exports) {
    exports('index', {
        queryLocalBaseUrl: queryLocalBaseUrl
    });
});