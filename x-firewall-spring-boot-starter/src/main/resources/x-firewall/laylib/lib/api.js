layui.define(function (exports) {
    function fetchSend(method, url, data) {
        let bootstrap
        if (method.toLowerCase() === 'post' || method.toLowerCase() === 'put') {
            bootstrap = fetch(url, {
                method: method,
                body: JSON.stringify(data),
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            })
        } else {
            bootstrap = fetch(`${url}?${new URLSearchParams(data).toString()}`, {
                method: method,
            })
        }

        return bootstrap.then(response => {
            return new Promise((resolve, reject) => {
                if (!response.ok) {
                    reject(response)
                } else {
                    var result = response.json();
                    resolve(result)
                }
            });
        })
    }

    let pathSplit = location.pathname.split("/x-firewall");
    let baseApi = pathSplit[0] + "/x-firewall"
    layui.jquery.ajaxSetup({
        complete: function (XMLHttpRequest, textStatus) {
            var res = XMLHttpRequest.responseText;
            try {
                var jsonData = JSON.parse(res);
                console.log(jsonData)
                if (jsonData.code === 403) {
                    localStorage.removeItem("x-firewall")
                    top.location.reload()
                }
            } catch (e) {
            }
        }
    });
    exports('api', {
        get: function (url, data) {
            return fetchSend('GET', baseApi + url, data)
        },
        post: function (url, data) {
            return fetchSend('POST', baseApi + url, data);
        }
    });
});