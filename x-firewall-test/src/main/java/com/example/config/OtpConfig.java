package com.example.config;


import com.nodemessage.x.core.otp.MfaOtpInstinct;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class OtpConfig {

    @Bean
    public MfaOtpInstinct mfaOtpInstinct() {
        return new MfaOtpInstinct(3, 1, TimeUnit.MINUTES);
    }

}
