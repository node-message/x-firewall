package com.example.config;

import com.nodemessage.x.spring.XFirewallFilterConfiguration;
import com.nodemessage.x.spring.config.AccessConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @author wjsmc
 * @date 2024/5/16 12:33
 * @description
 **/
@Configuration
public class XfireWallConfig implements XFirewallFilterConfiguration {
    @Override
    public void configAccess(AccessConfig accessConfig) {

    }
}
