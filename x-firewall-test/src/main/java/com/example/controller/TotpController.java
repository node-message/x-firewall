package com.example.controller;

import cn.hutool.captcha.generator.MathGenerator;
import com.nodemessage.x.core.baw.BlackAndWhiteDetailed;
import com.nodemessage.x.core.baw.BlackAndWhiteInstinct;
import com.nodemessage.x.core.otp.MfaOtpInstinct;
import com.nodemessage.x.core.totp.MfaTotpInstinct;
import com.nodemessage.x.core.totp.MfaTotpQrInfo;
import com.nodemessage.x.plugin.Plugin;
import com.nodemessage.x.plugin.PluginInstinct;
import com.nodemessage.x.plugin.PluginType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wjsmc
 * @date 2024/5/15 15:18
 * @description
 **/

@RequestMapping("totp")
@RestController
public class TotpController implements CommandLineRunner {

    @Autowired
    private MfaTotpInstinct mfaTotpInstinct;
    @Resource
    private MfaOtpInstinct mfaOtpInstinct;

    @Autowired
    private BlackAndWhiteInstinct blackAndWhiteInstinct;


    @GetMapping("bind")
    public ResponseEntity<String> bind() {
        String uid = "a";
        MfaTotpQrInfo wjsmc = mfaTotpInstinct.registerDevice(uid, "TEST:WJSMC", "wjsmc");
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(wjsmc.getQrCodeImage());
    }

    @GetMapping("verBind")
    public boolean verBind(String uid, String code) {
        return mfaTotpInstinct.registerVerifyDevice(uid, code);
    }

    @GetMapping("check")
    public boolean check(String uid, String code) {
        return mfaTotpInstinct.checkCode(uid, code);
    }

    @GetMapping("img")
    public void codeImg(HttpServletResponse response) throws IOException {
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        mfaOtpInstinct.randomCaptchaWrite("wjsmc", 120, 40, new MathGenerator(), response.getOutputStream());
    }

    @GetMapping("vcode")
    public boolean vcode(String code) throws IOException {
        return mfaOtpInstinct.verifyCaptcha("wjsmc", code);
    }


    @GetMapping("a")
    public boolean a(String code) throws IOException {
        return true;
    }

    @Override
    public void run(String... args) throws Exception {
        PluginInstinct load = PluginInstinct.Load(Plugin.builder()
                .path("E:\\com\\nodemessage\\x-firewall\\temp")
                .pluginType(PluginType.DIRECTORY)
                .build());
    }
}
